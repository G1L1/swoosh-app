//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Gilberto De La Garza on 2/22/20.
//  Copyright © 2020 Gilberto De La Garza. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor =
            UIColor.white.cgColor
    }

}
